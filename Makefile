application: libopencm3
	$(MAKE) -C application

libopencm3:
	$(MAKE) -C libopencm3

flash: application
	$(MAKE) flash -C application

clean: clean_application

clean_all: clean_application clean_opencm3

clean_application:
	$(MAKE) clean -C application

clean_opencm3:
	$(MAKE) clean -C libopencm3
