/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Lib C include group */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* Libopencm3 include group */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/systick.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>

/* FreeRTOS include group */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "lvgl.h"
#include "lv_conf.h"

#include "ili9341.h"

#include "debug.h"

#define PORT_LED GPIOA
#define PIN_LED GPIO6

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>

extern void vPortSVCHandler( void ) __attribute__ (( naked ));
extern void xPortPendSVHandler( void ) __attribute__ (( naked ));
extern void xPortSysTickHandler( void );

void sv_call_handler(void) {
	vPortSVCHandler();
}

void pend_sv_handler(void) {
	xPortPendSVHandler();
}
void sys_tick_handler(void) {
	xPortSysTickHandler();
} 

static void clock_setup(void)
{
	/* Enable GPIOC clock for LED & USARTs. */
	rcc_periph_clock_enable(RCC_GPIOA);

	/* Enable clocks for USART1. */
	rcc_periph_clock_enable(RCC_USART1);
}
static void gpio_setup(void)
{
	/* Setup GPIO pin GPIO4 on GPIO port A for LEDs. */
	gpio_mode_setup(PORT_LED, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_LED);
}

#define LVGL_TICK_TIMER pdMS_TO_TICKS(5) /*5ms*/
#define LVGL_REFRESH_RATE_MS pdMS_TO_TICKS(33) /*ms 33ms is around 30 action handling per seconds*/

static lv_disp_buf_t disp_buf;
static lv_color_t buf[LV_HOR_RES_MAX * LV_VER_RES_MAX / 10];                     /*Declare a buffer for 1/10 screen size*/
static TimerHandle_t lvgl_timer_handler = NULL;

static void _lvgl_tick(TimerHandle_t xTimer)
{
	lv_tick_inc(LVGL_TICK_TIMER);
}

static void _lvgl_task_handler_thread(void *param)
{
	/* Register the first wake up tick count, after that the vTaskDelayUntil will 
	 * take care of the update */
	TickType_t last_wakeup = xTaskGetTickCount();

	/* Init display driver */
	ili9341_init(); /*Init in a task because vTaskDelay in the init :x */

	while (1)
	{
		/* Let the lvgl lib do the work */
		lv_task_handler();

		/* Wait the time nescessarie to match the wanted ~30fps according to the time
		 * the screen painting/task handling did consume */
		vTaskDelayUntil(&last_wakeup, LVGL_REFRESH_RATE_MS);
	}
}

static void _task_led_blink(void *param)
{
	while(1)
	{
		/* Blink the led */
		gpio_toggle(PORT_LED, PIN_LED);

		/* 1sec delay */
		vTaskDelay(pdMS_TO_TICKS(500)/*ms*/);
	}
}

static void _lvgl_write_on_screen(lv_disp_drv_t * disp, const lv_area_t * area, lv_color_t * color_p)
{
	uint32_t nb_data = (((area->x2 + 1)  - area->x1) * ((area->y2+1) - area->y1));
	TickType_t start_time = xTaskGetTickCount();
	ili9341_set_pixel_frame(area->x1, area->x2, area->y1, area->y2);
	ili9341_send_pixel_data((uint16_t*)color_p, nb_data);
	printf("lvgl: x: %d -> %d, y: %d -> %d -- write %ld pixel - took %ld tick\n", area->x1, area->x2, area->y1, area->y2, nb_data, xTaskGetTickCount() - start_time);

    lv_disp_flush_ready(disp);         /* Indicate you are ready with the flushing*/
}

static void _btn_event_cb(lv_obj_t * btn, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        static uint8_t cnt = 0;
        cnt++;

        /*Get the first child of the button which is the label and change its text*/
        lv_obj_t * label = lv_obj_get_child(btn, NULL);
        lv_label_set_text_fmt(label, "Button: %d", cnt);
    }
}

/**
 * Create a button with a label and react on Click event.
 */
static void _lv_ex_get_started_1(void)
{
    lv_obj_t * btn = lv_btn_create(lv_scr_act(), NULL);     /*Add a button the current screen*/
    lv_obj_set_pos(btn, 10, 10);                            /*Set its position*/
    lv_obj_set_size(btn, 130, 50);                          /*Set its size*/
    lv_obj_set_event_cb(btn, _btn_event_cb);                 /*Assign a callback to the button*/

    lv_obj_t * label = lv_label_create(btn, NULL);          /*Add a label to the button*/
    lv_label_set_text(label, "Button");                     /*Set the labels text*/
}

static void _lv_ex_get_started_2(void)
{
    static lv_style_t style_btn;
    static lv_style_t style_btn_red;

    /*Create a simple button style*/
    lv_style_init(&style_btn);
    lv_style_set_radius(&style_btn, LV_STATE_DEFAULT, 10);
    lv_style_set_bg_opa(&style_btn, LV_STATE_DEFAULT, LV_OPA_COVER);
    lv_style_set_bg_color(&style_btn, LV_STATE_DEFAULT, LV_COLOR_SILVER);
    lv_style_set_bg_grad_color(&style_btn, LV_STATE_DEFAULT, LV_COLOR_GRAY);
    lv_style_set_bg_grad_dir(&style_btn, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);

    /*Swap the colors in pressed state*/
    lv_style_set_bg_color(&style_btn, LV_STATE_PRESSED, LV_COLOR_GRAY);
    lv_style_set_bg_grad_color(&style_btn, LV_STATE_PRESSED, LV_COLOR_SILVER);

    /*Add a border*/
    lv_style_set_border_color(&style_btn, LV_STATE_DEFAULT, LV_COLOR_WHITE);
    lv_style_set_border_opa(&style_btn, LV_STATE_DEFAULT, LV_OPA_70);
    lv_style_set_border_width(&style_btn, LV_STATE_DEFAULT, 2);

    /*Different border color in focused state*/
    lv_style_set_border_color(&style_btn, LV_STATE_FOCUSED, LV_COLOR_BLUE);
    lv_style_set_border_color(&style_btn, LV_STATE_FOCUSED | LV_STATE_PRESSED, LV_COLOR_NAVY);

    /*Set the text style*/
    lv_style_set_text_color(&style_btn, LV_STATE_DEFAULT, LV_COLOR_WHITE);

    /*Make the button smaller when pressed*/
    lv_style_set_transform_height(&style_btn, LV_STATE_PRESSED, -5);
    lv_style_set_transform_width(&style_btn, LV_STATE_PRESSED, -10);

    /*Add a transition to the size change*/
    static lv_anim_path_t path;
    lv_anim_path_init(&path);
    lv_anim_path_set_cb(&path, lv_anim_path_overshoot);

    lv_style_set_transition_prop_1(&style_btn, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_HEIGHT);
    lv_style_set_transition_prop_2(&style_btn, LV_STATE_DEFAULT, LV_STYLE_TRANSFORM_WIDTH);
    lv_style_set_transition_time(&style_btn, LV_STATE_DEFAULT, 300);
    lv_style_set_transition_path(&style_btn, LV_STATE_DEFAULT, &path);


    /*Create a red style. Change only some colors.*/
    lv_style_init(&style_btn_red);
    lv_style_set_bg_color(&style_btn_red, LV_STATE_DEFAULT, LV_COLOR_RED);
    lv_style_set_bg_grad_color(&style_btn_red, LV_STATE_DEFAULT, LV_COLOR_MAROON);
    lv_style_set_bg_color(&style_btn_red, LV_STATE_PRESSED, LV_COLOR_MAROON);
    lv_style_set_bg_grad_color(&style_btn_red, LV_STATE_PRESSED, LV_COLOR_RED);
    lv_style_set_text_color(&style_btn_red, LV_STATE_DEFAULT, LV_COLOR_WHITE);

    /*Create buttons and use the new styles*/
    lv_obj_t * btn = lv_btn_create(lv_scr_act(), NULL);     /*Add a button the current screen*/
    lv_obj_set_pos(btn, 10, 80);                            /*Set its position*/
    lv_obj_set_size(btn, 120, 50);                          /*Set its size*/
    lv_obj_reset_style_list(btn, LV_BTN_PART_MAIN);         /*Remove the styles coming from the theme*/
    lv_obj_add_style(btn, LV_BTN_PART_MAIN, &style_btn);

    lv_obj_t * label = lv_label_create(btn, NULL);          /*Add a label to the button*/
    lv_label_set_text(label, "Button 2");                     /*Set the labels text*/

    /*Create a new button*/
    lv_obj_t * btn2 = lv_btn_create(lv_scr_act(), btn);
    lv_obj_set_pos(btn2, 10, 140);
    lv_obj_set_size(btn2, 120, 50);                             /*Set its size*/
    lv_obj_reset_style_list(btn2, LV_BTN_PART_MAIN);         /*Remove the styles coming from the theme*/
    lv_obj_add_style(btn2, LV_BTN_PART_MAIN, &style_btn);
    lv_obj_add_style(btn2, LV_BTN_PART_MAIN, &style_btn_red);   /*Add the red style on top of the current */
    lv_obj_set_style_local_radius(btn2, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_RADIUS_CIRCLE); /*Add a local style*/

    label = lv_label_create(btn2, NULL);          /*Add a label to the button*/
    lv_label_set_text(label, "Button 3");                     /*Set the labels text*/
}

static lv_obj_t * label;
static void _slider_event_cb(lv_obj_t * slider, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        /*Refresh the text*/
        lv_label_set_text_fmt(label, "%d", lv_slider_get_value(slider));
    }
}
static void _lv_ex_get_started_3(void)
{
    /* Create a slider in the center of the display */
    lv_obj_t * slider = lv_slider_create(lv_scr_act(), NULL);
    lv_obj_set_width(slider, 200);                        /*Set the width*/
    lv_obj_set_pos(slider, 10, 200);    /*Align to the center of the parent (screen)*/
    lv_obj_set_event_cb(slider, _slider_event_cb);         /*Assign an event function*/

    /* Create a label below the slider */
    label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(label, "0");
    lv_obj_set_auto_realign(slider, true);                          /*To keep center alignment when the width of the text changes*/
    lv_obj_align(label, slider, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);    /*Align below the slider*/
}

static void _print_something_on_screen(void *param)
{
	vTaskDelay(3000);
	printf("Create button on screen\n");

	while (1)
	{
		_lv_ex_get_started_1();
		_lv_ex_get_started_2();
		_lv_ex_get_started_3();
		vTaskSuspend(NULL);
	}
}

static bool _my_touchpad_read(lv_indev_t * indev, lv_indev_data_t * data)
{
	int pressed = 0;
	uint16_t X = 0, Y = 0;

	pressed = ili9341_get_touch_coordinates(&X, &Y);

    data->state = pressed ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
    if(data->state == LV_INDEV_STATE_PR)
	{
		X = ((X * 240) / 4095);
		Y = ((Y * 320) / 4095);

//		printf("New X: %d, Y: %d\n", X, Y);
		data->point.x = X;
		data->point.y = Y;
	}

    return false; /*Return `false` because we are not buffering and no more data to read*/
}

int main(void)
{
	rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

	clock_setup();
	gpio_setup();

	/* Init debug to enable printf */
	debug_init();

	lv_init();

	lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * LV_VER_RES_MAX / 10);    /*Initialize the display buffer*/

	lv_disp_drv_t disp_drv;               /*Descriptor of a display driver*/
	lv_disp_drv_init(&disp_drv);          /*Basic initialization*/
	disp_drv.flush_cb = _lvgl_write_on_screen;    /*Set your driver function*/
	disp_drv.buffer = &disp_buf;          /*Assign the buffer to the display*/
	lv_disp_drv_register(&disp_drv);      /*Finally register the driver*/

	lv_indev_drv_t indev_drv;                  /*Descriptor of a input device driver*/
	lv_indev_drv_init(&indev_drv);             /*Basic initialization*/
	indev_drv.type = LV_INDEV_TYPE_POINTER;    /*Touch pad is a pointer-like device*/
	indev_drv.read_cb = _my_touchpad_read;      /*Set your driver function*/
	lv_indev_drv_register(&indev_drv);         /*Finally register the driver*/

	printf("Start led task\n\r");
	if(xTaskCreate(_task_led_blink, "blink", 512, NULL, 6, NULL) != pdPASS)
	{
		printf("Start led task FAILED\n\r");
	}

	printf("Create LVGL tick timer\n");
	lvgl_timer_handler = xTimerCreate("lvgl tick", LVGL_TICK_TIMER, pdTRUE, NULL, _lvgl_tick);
	if(lvgl_timer_handler == NULL)
	{
		printf("Create lvgl tick timer FAILED\n\r");
	}

	if(xTimerStart(lvgl_timer_handler, 0) != pdPASS)
	{
		printf("Start lvgl tick timer FAILED\n\r");
	}

	printf("Start lvgl tick task\n\r");
	if(xTaskCreate(_lvgl_task_handler_thread, "lvgl task handler", 1024, NULL, 6, NULL) != pdPASS)
	{
		printf("Start lvgl handler task FAILED\n\r");
	}

	if(xTaskCreate(_print_something_on_screen, "print", 1024, NULL, 6, NULL) != pdPASS)
	{
		printf("Start print task FAILED\n\r");
	}

	/* Start freeRTOS scheduler*/
	printf("Start FreeRTOS scheduler\n\r");
	vTaskStartScheduler();

	/* Shouln't end here, ever*/
	printf("scheduler EXITED\n\r");
	return 0;
}
