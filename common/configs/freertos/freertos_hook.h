#ifndef _FREERTOS_HOOK_H_
#define _FREERTOS_HOOK_H_

#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>

void vApplicationTickHook(void);
void vApplicationMallocFailedHook(void);
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName);

#endif /*_FREERTOS_HOOK_H_*/
