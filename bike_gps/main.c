/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <stdio.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "gps.h"
#include "gui.h"
#include "debug.h"

#define RCC_LED RCC_GPIOA
#define PORT_LED GPIOA
#define PIN_LED GPIO6

static void _task_led_blink(void *param)
{
	(void)param; /* avoid unused parameter warning */

	while(1)
	{
		/* Blink the led */
		gpio_toggle(PORT_LED, PIN_LED);

		/* 1sec delay */
		vTaskDelay(pdMS_TO_TICKS(500)/*ms*/);
	}
}

int main(void)
{
	rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

	/* Setup LED gpio */
	rcc_periph_clock_enable(RCC_LED);
	gpio_mode_setup(PORT_LED, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_LED);

	/* Init debug to enable printf */
	debug_init();

	if(xTaskCreate(_task_led_blink, "blink", 512, NULL, 6, NULL) != pdPASS)
	{
		printf("Start led task FAILED\n\r");
	}

	gui_init();

	gps_init();

	/* Start freeRTOS scheduler*/
	printf("Start FreeRTOS scheduler\n\r");
	vTaskStartScheduler();

	/* Shouln't end here, ever*/
	printf("scheduler EXITED\n\r");
	return 0;
}
