/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Lib C include group */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* Libopencm3 include group */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>

/* FreeRTOS include group */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "lvgl.h"
#include "lv_conf.h"

#include "ili9341.h"

#include "gui.h"
#include "debug.h"

#define LVGL_TICK_TIMER pdMS_TO_TICKS(5) /*5ms*/
#define LVGL_REFRESH_RATE_MS pdMS_TO_TICKS(33) /*ms 33ms is around 30 action handling per seconds*/

static lv_disp_buf_t disp_buf;
static lv_color_t buf[LV_HOR_RES_MAX * LV_VER_RES_MAX / 10];                     /*Declare a buffer for 1/10 screen size*/
static TimerHandle_t lvgl_timer_handler = NULL;
static lv_disp_drv_t disp_drv;
static lv_indev_drv_t indev_drv;

static void _lvgl_tick(TimerHandle_t xTimer)
{
	(void)xTimer; /* avoid unused parameter warning */
	lv_tick_inc(LVGL_TICK_TIMER);
}

static void _lvgl_task_handler_thread(void *param)
{
	/* Register the first wake up tick count, after that the vTaskDelayUntil will 
	 * take care of the update */
	TickType_t last_wakeup = xTaskGetTickCount();

	(void)param; /* avoid unused parameter warning */

	/* Init display driver */
	ili9341_init(); /*Init in a task because vTaskDelay in the init */

	while (1)
	{
		/* Let the lvgl lib do the work */
		lv_task_handler();

		/* Wait the time nescessarie to match the wanted ~30fps according to the time
		 * the screen painting/task handling did consume */
		vTaskDelayUntil(&last_wakeup, LVGL_REFRESH_RATE_MS);
	}
}

static void _lvgl_write_on_screen(lv_disp_drv_t * disp, const lv_area_t * area, lv_color_t * color_p)
{
	uint32_t nb_data = (((area->x2 + 1)  - area->x1) * ((area->y2+1) - area->y1));
	TickType_t start_time = xTaskGetTickCount();
	ili9341_set_pixel_frame(area->x1, area->x2, area->y1, area->y2);
	ili9341_send_pixel_data((uint16_t*)color_p, nb_data);
	//printf("lvgl: x: %d -> %d, y: %d -> %d -- write %ld pixel - took %ld tick\n", area->x1, area->x2, area->y1, area->y2, nb_data, xTaskGetTickCount() - start_time);

	/* Indicate you are ready with the flushing*/
	lv_disp_flush_ready(disp);
}

static bool _touchpad_read(struct _lv_indev_drv_t * indev, lv_indev_data_t * data)
{
	int pressed = 0;
	uint16_t X = 0, Y = 0;

	(void)indev; /* avoid unused parameter warning */

	pressed = ili9341_get_touch_coordinates(&X, &Y);

    data->state = pressed ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
    if(data->state == LV_INDEV_STATE_PR)
	{
		X = ((X * 240) / 4095);
		Y = ((Y * 320) / 4095);

//		printf("New X: %d, Y: %d\n", X, Y);
		data->point.x = X;
		data->point.y = Y;
	}

    return false; /*Return `false` because we are not buffering and no more data to read*/
}

typedef struct line {
	lv_point_t line_points[2];
} T_line;

/* Build the basic demo UI */
static int _build_ui(void)
{
	/* Array of point for the line between label */
	static T_line line_list[] = {
		/*Horizontal line*/
		{.line_points = {{0, 80}, {240, 80}}},
		{.line_points = {{0, 160}, {240, 160}}},
		{.line_points = {{0, 240}, {240, 240}}},
		/*Vertical line*/
		{.line_points = {{120, 80}, {120, 160}}},
		{.line_points = {{120, 160}, {120, 240}}},
		{.line_points = {{120, 240}, {120, 320}}}};

	/*Style of the line*/
	static lv_style_t style_line;
	lv_style_init(&style_line);
	lv_style_set_line_width(&style_line, LV_STATE_DEFAULT, 2);
	lv_style_set_line_color(&style_line, LV_STATE_DEFAULT, LV_COLOR_BLUE);
	lv_style_set_line_rounded(&style_line, LV_STATE_DEFAULT, true);

	int nb_line = sizeof(line_list)/sizeof(line_list[0]);
	for(int line_index = 0; line_index < nb_line; line_index++)
	{
		/*Create a line and apply the new style*/
		lv_obj_t * line1;
		line1 = lv_line_create(lv_scr_act(), NULL);
		lv_line_set_points(line1, line_list[line_index].line_points, 2);     /*Set the points*/
		lv_obj_add_style(line1, LV_LINE_PART_MAIN, &style_line);     /*Set the points*/
		//lv_obj_align(line1, NULL, LV_ALIGN_CENTER, 0, 0);
	}

	/*Heart Rate label*/
	lv_obj_t * label_hr = lv_label_create(lv_scr_act(), NULL);
	lv_obj_set_auto_realign(label_hr, true);
	lv_obj_align(label_hr, NULL, LV_ALIGN_IN_TOP_MID, 0, 5);
	lv_obj_set_height(label_hr, 80);
	lv_label_set_align(label_hr, LV_LABEL_ALIGN_CENTER); /*Align text center*/
	lv_label_set_text(label_hr, "Heart Rate\n_ _ _ bpm\n");

	return 0;
}

int gui_init(void)
{
	int ret = 0;

	/* Basic lvgl init */
	lv_init();

	/* Init the display buffer */
	lv_disp_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * LV_VER_RES_MAX / 10);

	/* Init the screen driver */
	lv_disp_drv_init(&disp_drv);
	disp_drv.flush_cb = _lvgl_write_on_screen;
	disp_drv.buffer = &disp_buf;
	lv_disp_drv_register(&disp_drv);

	/* Init the input driver (touchpad) */
	lv_indev_drv_init(&indev_drv);
	indev_drv.type = LV_INDEV_TYPE_POINTER;
	indev_drv.read_cb = _touchpad_read;
	lv_indev_drv_register(&indev_drv);

	lvgl_timer_handler = xTimerCreate("lvgl tick", LVGL_TICK_TIMER, pdTRUE, NULL, _lvgl_tick);
	if(lvgl_timer_handler == NULL)
	{
		printf("Create lvgl tick timer FAILED\n\r");
		return -1;
	}

	if(xTimerStart(lvgl_timer_handler, 0) != pdPASS)
	{
		printf("Start lvgl tick timer FAILED\n\r");
		return -2;
	}

	if(xTaskCreate(_lvgl_task_handler_thread, "lvgl task handler", 1024, NULL, 6, NULL) != pdPASS)
	{
		printf("Start lvgl handler task FAILED\n\r");
		return -3;
	}

	ret = _build_ui();
	if(ret < 0)
	{
		printf("Build UI fail, return %d\n", ret);
		return -4;
	}

	return 0;
}
