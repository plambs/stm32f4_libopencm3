/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Libopencm3 include group */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#define PORT_LED GPIOA
#define PIN_LED GPIO6

int main(void)
{
	/* Enable GPIOC clock for LED. */
	rcc_periph_clock_enable(RCC_GPIOA);

	/* Setup GPIO pin GPIO6 on GPIO port A for LEDs. */
	gpio_mode_setup(PORT_LED, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_LED);

	while(1){
		/* Blink the led */
		gpio_toggle(PORT_LED, PIN_LED);

		/* Add some delay */
		for(uint32_t i = 0; i < 1000000; i++)
		{ 
			__asm__("nop");
		}
	}
	
	return 0;
}
