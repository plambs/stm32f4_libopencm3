/*
Copyright (C) 2020 LAMBS Pierre-Antoine

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Lib C include group */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* Libopencm3 include group */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/systick.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>

/* FreeRTOS include group */
#include "FreeRTOS.h"
#include "task.h"

#include "debug.h"

#define PORT_LED GPIOA
#define PIN_LED GPIO6

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>

extern void vPortSVCHandler( void ) __attribute__ (( naked ));
extern void xPortPendSVHandler( void ) __attribute__ (( naked ));
extern void xPortSysTickHandler( void );

void sv_call_handler(void) {
	vPortSVCHandler();
}

void pend_sv_handler(void) {
	xPortPendSVHandler();
}
void sys_tick_handler(void) {
	xPortSysTickHandler();
} 

static void clock_setup(void)
{
	/* Enable GPIOC clock for LED & USARTs. */
	rcc_periph_clock_enable(RCC_GPIOA);

	/* Enable clocks for USART1. */
	rcc_periph_clock_enable(RCC_USART1);
}
static void gpio_setup(void)
{
	/* Setup GPIO pin GPIO4 on GPIO port A for LEDs. */
	gpio_mode_setup(PORT_LED, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, PIN_LED);
}

static void _task_log_uart(void *param)
{
	uint8_t counter = 0;
	while (1)
	{
		printf("loop %d\n\r", counter);
		counter++;

		vTaskDelay(2000);
	}
}

static void _task_led_blink(void *param)
{
	while(1)
	{
		/* Blink the led */
		printf("toogle led\r\n");
		gpio_toggle(PORT_LED, PIN_LED);

		/* 1sec delay */
		vTaskDelay(1000);
	}
}

int main(void)
{
	rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

	clock_setup();
	gpio_setup();

	/* Init debug to enable printf */
	debug_init();

	printf("Start led task\n\r");
	if(xTaskCreate(_task_led_blink, "blink", 512, NULL, 6, NULL) != pdPASS)
	{
		printf("Start led task FAILED\n\r");
	}

	printf("Start uart task\n\r");
	if(xTaskCreate(_task_log_uart, "log", 1024, NULL, 6, NULL) != pdPASS)
	{
		printf("Start uart task FAILED\n\r");
	}

	/* Start freeRTOS scheduler*/
	printf("Start FreeRTOS scheduler\n\r");
	vTaskStartScheduler();

	/* Shouln't end here, ever*/
	printf("scheduler EXITED\n\r");
	return 0;
}
