# Example of stm32f407 application using libopencm3.

## Requirement
* compiler, something like arm-none-eabi-gcc.
* usb to ttl adapter to view the uart log.
* stm32f407 eval board (ex: STM32F4VE).
* stlinkV2 to flash the board.

## Instructions
 1. git clone --recurse-submodules https://gitlab.com/papalou/stm32f4\_libopencm3.git
 2. cd stm32f4\_libopencm3
 3. make -C libopencm3 # (Only needed once)
 4. make -C project
 5. make flash -C project

If you have an older git, or got ahead of yourself and skipped the ```--recurse-submodules```
you can fix things by running ```git submodule update --init``` (This is only needed once)

## Directories
* doc: some datasheet and pinout/picture of the board.
* blinky: simple blinky application that use libopencm3 as HAL.
* minimal_freertos: example of freertos using libopencm3 as HAL, with 2 task, one blink the led,
  the second print a message on the uart.
* lvgl_demo: minimal lvgl example (2 button and 1 slider) using freertos, the lvgl lib and the display/touchscreen
  driver.
* common: all code shared between project (uart logging, freertos, config...).

## License
All 3rd party code with their own license are in common/3rd_party except for the libopencm3 code that is in a separate folder.
All other code is released under the GPLv3, see LICENSE.md.
